CILAB Repo
===

[![pipeline status](https://gitlab.com/mdvillalobos/cilab/badges/master/pipeline.svg)](https://gitlab.com/mdvillalobos/cilab/commits/master)

[![coverage report](https://gitlab.com/mdvillalobos/cilab/badges/master/coverage.svg)](https://gitlab.com/mdvillalobos/cilab/commits/master)


An demo repository to test Gitlab DevOps feature and CI/CD Pipelines.

### Status
Current project defines a gitlab-ci.yaml file with two environments Staging & Production. 

The pipeline is connected to Heroku and deploys automatically to Staging and Manually to Prod


